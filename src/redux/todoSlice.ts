import { createSlice } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit';

export interface TodoState {
	id: string;
	title: string;
	completed: boolean;
}

const initialState: TodoState[] = [];

export const todoSlice = createSlice({
	name: 'todos',
	initialState,
	reducers: {
		add: (state, action: PayloadAction<TodoState>) => {
			return [...state, action.payload];
		},

		check: (state, action: PayloadAction<string>) => {
			const todoIndex = state.findIndex((el) => el.id === action.payload);
			if (todoIndex !== -1) {
				state[todoIndex].completed = !state[todoIndex].completed;
			}
			return state;
		},

		edit: (state, action: PayloadAction<TodoState>) => {
			const todoIndex = state.findIndex((el) => el.id === action.payload.id);
			if (todoIndex !== -1) {
				state[todoIndex] = action.payload;
			}
			return state;
		},

		remove: (state, action: PayloadAction<string>) => {
			return state.filter((el) => el.id !== action.payload);
		},

		get: (state, action: PayloadAction<TodoState[]>) => {
			state = [...action.payload];
			return state;
		},
	},
});

export const { add, check, edit, remove, get } = todoSlice.actions;

export default todoSlice.reducer;
