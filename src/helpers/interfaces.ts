export interface ItemProps {
	id: string;
	title: string;
	completed: boolean;
}

export interface FormProps {
	handleSubmit: (e: any) => void;
	handleChange: (e: any) => void;
	inputText: string;
}
