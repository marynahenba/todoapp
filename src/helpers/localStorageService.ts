export const getStoredTodos = (): any[] => {
	const storageTodos = localStorage.getItem('todos');
	const todosArr = storageTodos !== null ? JSON.parse(storageTodos) : [];
	return todosArr;
};

export const storeToLocalStorage = (newTodos: any): void => {
	const storedTodos = localStorage.getItem('todos');
	const todosArr = storedTodos !== null ? JSON.parse(storedTodos) : [];
	todosArr.push(newTodos);
	localStorage.setItem('todos', JSON.stringify(todosArr));
};

export const updateStorage = (todos: any[]): void => {
	localStorage.clear();
	localStorage.setItem('todos', JSON.stringify(todos));
};

export const removeFromStorage = (id: string): void => {
	const storedTodos = localStorage.getItem('todos');
	const todosArr = storedTodos !== null ? JSON.parse(storedTodos) : [];
	const filtered = todosArr.filter((el: any) => el.id !== id);
	localStorage.setItem('todos', JSON.stringify(filtered));
};
