import React from 'react';
import { useSelector } from 'react-redux';
import type { RootState } from '../redux/store';
import InputForm from './InputForm';
import TodoList from './TodoList';
import { Container } from '../styles/Todo.styled';

export default function Todo(): JSX.Element {
	const todos = useSelector((state: RootState) => state.todos);

	return (
		<Container>
			<div className='title'>
				<h1>
					Todos (<span>{todos.length}</span>)
				</h1>
			</div>
			<div className='wrapper'>
				<InputForm />
				<TodoList />
			</div>
		</Container>
	);
}
