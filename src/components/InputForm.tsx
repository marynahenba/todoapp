import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { nanoid } from 'nanoid';
import { add } from '../redux/todoSlice';
import { storeToLocalStorage } from '../helpers/localStorageService';
import { Form } from '../styles/Form.styled';

export default function InputForm(): JSX.Element {
	const [inputText, setInputText] = useState('');
	const dispatch = useDispatch();

	const handleSubmit = (e: any): void => {
		e.preventDefault();
		if (inputText.length > 0) {
			const newTodos = {
				id: nanoid(),
				title: inputText,
				completed: false,
			};
			dispatch(add(newTodos));
			setInputText('');
			storeToLocalStorage(newTodos);
		} else {
			alert('Enter the title of your todo');
		}
	};

	const handleChange = (e: any): void => {
		e.preventDefault();
		setInputText(e.target.value);
	};

	return (
		<Form onSubmit={handleSubmit}>
			<input
				type='text'
				placeholder='Enter todo here'
				value={inputText}
				onChange={handleChange}
			/>
			<button type='submit'>Submit</button>
		</Form>
	);
}
