import React from 'react';
import { FormProps } from '../helpers/interfaces';

export default function EditForm(props: FormProps): JSX.Element {
	return (
		<form onSubmit={props.handleSubmit}>
			<input
				type='text'
				value={props.inputText}
				onChange={props.handleChange}
			/>
			<button className='editBtn' type='submit'>
				Edit
			</button>
		</form>
	);
}
