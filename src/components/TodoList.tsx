import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import type { RootState } from '../redux/store';
import { get } from '../redux/todoSlice';
import { getStoredTodos } from '../helpers/localStorageService';
import TodoItem from './TodoItem';
import { List } from '../styles/List.styled';

export default function TodoList(): JSX.Element {
	const todos = useSelector((state: RootState) => state.todos);
	const dispatch = useDispatch();

	useEffect(() => {
		const todosArr = getStoredTodos();
		dispatch(get(todosArr));
	}, []);

	return (
		<div>
			<List>
				{todos.length !== 0 &&
					todos.map((task) => {
						return (
							<TodoItem
								key={task.id}
								id={task.id}
								title={task.title}
								completed={task.completed}
							/>
						);
					})}
			</List>
		</div>
	);
}
