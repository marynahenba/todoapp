import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { FaRegEdit } from 'react-icons/fa';
import { ImBin } from 'react-icons/im';
import { check, edit, remove } from '../redux/todoSlice';
import { RootState } from '../redux/store';
import {
	removeFromStorage,
	updateStorage,
} from '../helpers/localStorageService';
import EditForm from './EditForm';
import { ItemProps } from '../helpers/interfaces';
import { ListItem } from '../styles/Item.styled';

export default function TodoItem(props: ItemProps): JSX.Element {
	const [editingMode, setEditingMode] = useState(false);
	const [inputText, setInputText] = useState(props.title);
	const todos = useSelector((state: RootState) => state.todos);
	const dispatch = useDispatch();

	useEffect(() => {
		updateStorage(todos);
	}, [todos]);

	const handleSubmit = (e: any): void => {
		e.preventDefault();
		const updatedTodo = {
			id: props.id,
			title: inputText,
			completed: props.completed,
		};
		dispatch(edit(updatedTodo));
		setEditingMode(false);
	};

	const handleChange = (e: any): void => {
		e.preventDefault();
		setInputText(e.target.value);
		updateStorage(todos);
	};

	const handleCheck = (e: any): void => {
		dispatch(check(props.id));
		updateStorage(todos);
	};

	return (
		<ListItem>
			<div>
				{editingMode ? (
					<EditForm
						handleSubmit={handleSubmit}
						handleChange={handleChange}
						inputText={inputText}
					/>
				) : (
					<>
						<input
							className='checkbox'
							type='checkbox'
							onChange={handleCheck}
							checked={props.completed}
						/>
						{props.title}
					</>
				)}
			</div>
			<div>
				<button
					className='editBtn'
					type='button'
					onClick={() => setEditingMode(true)}
				>
					<FaRegEdit />
				</button>
				<button
					className='deleteBtn'
					type='button'
					onClick={() => {
						dispatch(remove(props.id));
						removeFromStorage(props.id);
					}}
				>
					<ImBin />
				</button>
			</div>
		</ListItem>
	);
}
