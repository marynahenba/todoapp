import styled from 'styled-components';

export const ListItem = styled.li`
	display: flex;
	justify-content: space-between;
	padding: 10px;
	background-color: #f2f2f2;

	:nth-child(2n) {
		background-color: #fff;
	}

	.checkbox {
		margin-right: 10px;
	}

	.editBtn,
	.deleteBtn {
		cursor: pointer;
		color: #fff;
		border-radius: 2px;
		padding: 5px 8px;
	}

	.editBtn {
		border: 1px solid #54a451;
		background-color: #54a451;
		margin-right: 10px;
	}

	.deleteBtn {
		border: 1px solid #cb444a;
		background-color: #cb444a;
	}
`;
