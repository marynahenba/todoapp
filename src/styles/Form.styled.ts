import styled from 'styled-components';

export const Form = styled.form`
	display: flex;
	align-items: center;
	justify-content: space-between;
	margin-bottom: 15px;

	input {
		width: 100%;
	}

	input::placeholder {
		font-size: 17px;
	}

	button {
		border: 1px solid #3779f6;
		border-radius: 2px;
		color: #fff;
		cursor: pointer;
		padding: 5px 15px;
		background-color: #3779f6;
	}
`;
