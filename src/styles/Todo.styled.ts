import styled from 'styled-components';

const Container = styled.div`
	max-width: 600px;
	margin: 40px auto;
	background-color: #fff;
	border: 1px solid #ededef;

	.title {
		padding: 10px;
		background-color: #f7f7f7;
		border: 1px solid #ededef;
	}

	h1 {
		font-size: 25px;
	}

	.wrapper {
		margin: 15px 20px 20px;
	}
`;

export { Container };
