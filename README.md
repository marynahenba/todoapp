# Getting Started with Create React App

Todo App implemented with React, Redux-toolkit, Typescript, Styled-Components.

## Available Scripts

### `npm start`

Runs the app in the development mode.\

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.
